# for statements
# different from standard c for loop 

# create list of ints
list_int_memory= [1,2,4,8,16,32,64,128]
# print all items in the list 
for ints in list_int_memory:
    print(ints)

# create list of stirngs
list_string_git_operators = ['git init','git add .','git commit -a','git log -p']

# print all items in list of strings 
for git in list_string_git_operators:
    print(git,len(git))

# print slice of the int list
#NOTE 2 includes -1 excludes 
for ints in list_int_memory[2:-1]:
    print(ints)
