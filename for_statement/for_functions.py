

# range range(from , to excluded, step ) 

for i in range(0,10,5):
    print(i)


# reversed range 
# same as range but in reverse 
for i in reversed(range(0,10,1)):
        print(i)

# sorted 
# sorting a set


set_cnc = ['mill','lathe','drill press','3d printer']

for i in sorted(set(set_cnc)):
        print(i)






