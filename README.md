# python_sintaksa
Sintaksa za Monty Python programski jezik 

![Python](img/mp.jpeg?raw=true "Title")

### - Virtualno py okruzenje 

- installacija 
```
$ sudo apt update
$ sudo apt install virtualenv
```

- koriscenje
```
$ mkdir neki_py_folder && cd $_
```
- napravi virtualno py okruzenje npr za open cv 
```
$ virtualenv opencvvrt
```
- ulaz 
```
$ source opencvvrt/bin/activate
```
- izlaz 
```
(opencvvirt)user@user: deactivate
```
- instalacija standardno pomocu pip 
```
pip install -U pip
pip install <package>
```



### - Merenje performansi 

- step 1 install kcachegrind
```
# apt get kcachegrind
```

- step 2 import cProfile 
```
import cProfile
```
- step 3 generisi file sa podacima o performansama (2 nacina )
   ```
   python -m cProfile -o performanse.cprof -s tottime uniformna_distribucija.py
   
   -o je output file 
   -s je sortiranje po koloni 
   - cProfile https://docs.python.org/3/library/profile.html
   ```
   
   ili
   
   ```
   python -m cProfile -s tottime uniformna_distribucija.py > performanse.tabela
   ```
   
- step 4 uzmi .perf file i prikazi ga u kcachegrind-u
     
   ```
   kcachegrind performanse.perf
   ```


### -Funkcije i klase iz modula 
```print(dir(NEKI_MODUL)) ```

    from mxnet import autograd
    ```print(dir(autograd)) ```
### -Help funkcija tj sta radi ta komanda ? 
```help()```

### -Rezervisano za python 

\_\_file\_\_ specijalni objekti u pythonu

\_Null\_ interne funkcije 


### -Udji u py interpreter - **PAZI NA VERZIJU !!!** URADI ```python --version ```
```python3``` ili ```python``` ili ```python2```

### -Python lokacija binary / source-a i man strane 
```$ whereis python ``` 

### -Lokacija komande  

    ```$ which python ``` 
      /usr/bin/python

### -Exit python interpreter terminal
```>>> quit()``` or ```Ctrl+D```

### -UNIX “shebang” line
```#!/usr/bin/env python3.5```

### -Stil kodiranja po PEP-u

module_name, package_name, ClassName, method_name, ExceptionName, function_name, GLOBAL_CONSTANT_NAME, global_var_name, instance_var_name, function_parameter_name, local_var_name

![Python](img/mp.gif?raw=true "Title")

https://www.python.org/dev/peps/pep-0008/
- Use 4-space indentation, and no tabs
- Wrap lines so that they don’t exceed 79 characters.
- Use blank lines to separate functions and classes, and larger blocks of code inside functions.
- When possible, put comments on a line of their own.
- Use docstrings.
- Use spaces around operators and after commas, but not directly inside bracketing constructs: a = f(1, 2) + g(3, 4).
- Name your classes and functions consistently; 
- CamelCase for classes  
- lower_case_with_underscores for functions and methods
- Variable names follow the same convention as function names.


### -Shalow vs deep copy 
![Shalow vs deep ](img/ShallowCopy_DeepCopy.jpg?raw=true "Title")


