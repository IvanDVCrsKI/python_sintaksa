

#lambda function

# format 
# lambda arguments : expression 

hl = lambda a:a +1 
print hl(2)


hl3 = lambda hl1,hl2 : hl1+hl2
print(hl3(1,2))


def valve_pls(addOneAndTwo):
    return lambda hl : hl+addOneAndTwo

hl3=valve_pls(2)
print(hl3(1))

# where python lambda functions are usefull
# example code 1
# find numbers divisable by 3 in list 
# standard way

def fun_filter(int_x):
    return int_x % 3 == 0

list_of_ints_divisable_by_3 = filter(fun_filter,[1,2,3,4,5,6,7,8,9,12])
print(list_of_ints_divisable_by_3)


#the way of lambda 
list_lambda_divisable_by_3 = filter(lambda int_x : int_x % 3==0,[1,2,3,4,5,6,7,8,9,12])

print(list_lambda_divisable_by_3)

# lambdas are functions 
# f = lambda x : x +1 is same as 
# f = function(x){x+1}


