# creating empty set the right way 
# sets are mutable 
set_mem=set()

# set() will create empty set 
# {} this will not create set it will create dictionary
# a = set is bad 
# {0}-{0} the funny way , {} is dictionary, {0} is set, - is difference  


print(set_mem)
print({0}-{0})

set_latin = {'per',' aspera', 'ad', 'astra'}
print(set_latin)

#check if item exists in set and  return true of false
print('per' in set_latin)
print('veni' in set_latin)

#set operations 
set_a = set('test')
set_b = set('zebra')

set_movies = {'Aliens','Star wars', 'Aliens', '5th element','Linux'} 
set_os = {'Linux','Windows 7','Aliens','Plan9'}



#unuque letters in set 
print(set_a)
print(set_movies)


# letters tht are in set a but not b 
print(set_a-set_b)
print(set_movies - set_os)

# leters that are in a or b or both 
print(set_a | set_b)
print(set_movies | set_os)

# letters in both a and b 
print(set_a & set_b)
print(set_movies & set_os)

# letters in a or b but not both 
print(set_a ^ set_b) 
print(set_movies ^ set_os)















